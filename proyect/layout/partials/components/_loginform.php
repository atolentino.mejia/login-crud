<div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
            <!--begin::Signin-->
            <div class="form-floating">
                <div class="text-center mb-10 mb-lg-20">
                    <h3 class="font-size-h1">Inicio de sesion</h3>
                    <p class="text-muted font-weight-bold">Ingresa tus credenciales</p>
                </div>
                <!--begin::Form-->
                <div class="responseve ">
                <form class="form fv-plugins-bootstrap fv-plugins-framework " novalidate="novalidate" id="kt_login_signin_form">
                    <div class="form-group fv-plugins-icon-container p-3">
                        <input required class="form-control form-control h-auto py-5 px-6" type="text" placeholder="Nombre de Usuario" name="username" id="username" autocomplete="off">
                    <div class="fv-plugins-message-container"></div></div>
                    <div class="form-group fv-plugins-icon-container p-3">
                        <input required class="form-control form-control h-auto py-5 px-6" type="password" placeholder="Contraseña" name="password" id="password" autocomplete="off">
                    <div class="fv-plugins-message-container"></div></div>
                    <!--begin::Action-->
                    <button type="submit" id="kt_login_signin_submit" class="btn btn-primary container ">Inicio de sesion</button>
                    <!--end::Action-->
                <input type="hidden"><div></div></form>
                <!--end::Form-->
            </div></div>
            <!--end::Signin-->
        </div>